#
# crop_image.py
#
#	This file is part of Freedroid
#
#	Freedroid is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	(at your option) any later version.
#
#	Freedroid is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with Freedroid; see the file COPYING. If not, write to the 
#	Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
#	MA	02111-1307	USA
#
# HOW TO USE THE SCRIPT:	The script is linked to the frame change. All that needs to
#				be done is press the 'Animation' button.
#
#
# Note:
# 1) The script will write result images inside the directory specified in
#	target_file_prefix. This directory must exist before the script is run
#
# 2) Adapt 'croppy_path' definition to your local installation
#
# 3) The variables under croppy_path need to be adjusted; notice that
#	target_file_prefix must end with iso_ , and that body_part_string
#	is a list and can have several obstacle names if more than one
#	obstacle is in the file.
#
# 4) Set the animation range to begin at frame 1 and to end one frame
#	after all obstacles have been rendered from all angles. Usually this
#	(4 * (number of obstacles in the file)).
#
# 5) If using this script for the first time, PLEASE REFER TO
# http://www.freedroid.org/pmwiki/pmwiki.php/Main/RenderingGraphics
#		for usage instructions and explanations.


######################################################################
######################### PREPARE EVERYTHING #########################
######################################################################

######################
# Let's start with the preparations:	Importing modules...
#
import bpy
import os
import sys
import mathutils
from math import *
from bpy import *


######################
# Define some keywords and pathes that will affect
# output file generation...
#
croppy_path = "~/freedroid/croppy/croppy"

source_file_prefix = bpy.context.scene.render.filepath

target_file_prefix = "~/freedroid/graphics/tmp/iso_"
body_part_string = ["obst1", "obst2"]

#######################################################################
########################### offset ####################################
#######################################################################

def compute_default_frustum(near, far, lens, aspect_ratio):
	half_size = 16 * near / lens
	if aspect_ratio > 1:
		size_x = half_size
		size_y = half_size / aspect_ratio
	else:
		size_x = half_size * aspect_ratio
		size_y = half_size
	return -size_x, size_x, -size_y, size_y, near, far

def compute_default_ortho(near, far, scale, aspect_ratio):
	half_size = scale / 2
	if aspect_ratio > 1:
		size_x = half_size
		size_y = half_size / aspect_ratio
	else:
		size_x = half_size * aspect_ratio
		size_y = half_size
	return -size_x, size_x, -size_y, size_y, -far, near

def get_frustum_matrix(left, right, bottom, top, near, far):
	return mathutils.Matrix([
		[2.0 * near / (right - left), 0.0, (right + left) / (right - left), 0.0],
		[0.0, 2.0 * near / (top - bottom), (top + bottom) / (top - bottom), 0.0],
		[0.0, 0.0, - (far + near) / (far - near), - (2 * far * near) / (far - near)],
		[0.0, 0.0, -1.0, 0.0]])

def get_ortho_matrix(left, right, bottom, top, near, far):
	return mathutils.Matrix([
		[2.0 / (right - left), 0.0, 0.0, - (right + left) / (right - left)],
		[0.0, 2.0 / (top - bottom), 0.0, - (top + bottom) / (top - bottom)],
		[0.0, 0.0, -2.0 / (far - near), -(far + near) / (far - near)],
		[0.0, 0.0, 0.0, 1.0]])

def get_perspective_matrix(camera, aspect_ratio):
	if camera.type == "ORTHO":
		left, right, bottom, top, near, far = compute_default_ortho(camera.clip_start, camera.clip_end, camera.ortho_scale, aspect_ratio)
		return get_ortho_matrix(left, right, bottom, top, near, far)
	elif camera.type == "PERSP":
		left, right, bottom, top, near, far = compute_default_frustum(camera.clip_start, camera.clip_end, camera.lens, aspect_ratio)
		return get_frustum_matrix(left, right, bottom, top, near, far)

def write_offset(scene, filename):
	res_x = scene.render.resolution_x
	res_y = scene.render.resolution_y
	pix_asp_x = scene.render.pixel_aspect_x
	pix_asp_y = scene.render.pixel_aspect_y

	aspect_ratio = 1.0
	if res_y != 0:
		aspect_ratio = (res_x * pix_asp_x) / (res_y * pix_asp_y)

	camera = bpy.data.objects["RotoCamera"]
	camera_mat = camera.matrix_world.copy()
	camera_mat.invert()

	camera = camera.data
	persp_mat = get_perspective_matrix(camera, aspect_ratio)

	offset_object = bpy.data.objects["offset"]
	offset_pos = offset_object.location.copy()
	offset_point = persp_mat * (camera_mat * offset_pos)
	xnd = offset_point.x
	ynd = offset_point.y
	center_x, center_y = (xnd + 1) * (res_x / 2.0), res_y - (ynd + 1) * (res_y / 2.0)
	offset_x, offset_y = -round(center_x), -round(center_y)

	with open(filename, 'w') as offset_file:
		offset_file.write("OffsetX=" + str(offset_x) + "\n")
		offset_file.write("OffsetY=" + str(offset_y) + "\n")

######################################################################
###################### PREPARE RENDERING FUNCTION ####################
######################################################################

def prepare_rendering(scene):
	framenb = scene.frame_current
	print("... prepare_rendering frame " + str(framenb))
	write_offset(scene, source_file_prefix + format(scene.frame_current, "04") + ".offset")

######################################################################
######################## POST RENDERING FUNCTION #####################
######################################################################

def post_rendering(scene):
	framenb = scene.frame_current
	print("... post_rendering frame " + str(framenb))

	source_filename = source_file_prefix + format(framenb, "04") + ".png"
	offset_filename = source_file_prefix + format(framenb, "04") + ".offset"

	# The final image name is composed of: (1) The path to the image, ending with "iso_",
	# which leads to (2) the actual name of the obstacle, and finally (3) the cardinal
	# direction (w, s, e, n) the obstacle is facing in the image. (1) was set as a var early
	# in the script. Here we determine (2) and (3).
	
	# There might be several obstacles in this file, which should be listed in order	of
	# rendering in body_part_string. Each has 4 directions it's rendered from, and its own
	# name. We divide the frame number by 4 to get the number in the index of
	# the obstacle being rendered. We subtract 0.1 because e.g. int(4/4) = 1, but in frame 4
	# we're still rendering the object with the index 0.
	obstacle_name = body_part_string[int(framenb / 4 - 0.1)]
	
	# If everything is set up right, each obstacle is facing west according to the ingame
	# compass (negative y usually), and turns -90 degrees each frame to face south, east
	# and north. Thus we use modulo 4 to know the object's orientation.
	direction = ['e', 's', 'w', 'n'][(framenb - 1) % 4]
	
	target_filename = target_file_prefix + obstacle_name + "_" + direction + ".png"

	os.system(croppy_path + " -i " + source_filename + " -o " + target_filename)

	os.system("rm " + source_filename)
	os.system("rm " + offset_filename)

######################################################################
########################## 'MAIN PROGRAM' ############################
######################################################################

# Clear the list of functions to be run before and after every frame is rendered;
# thus it is not necessary to reload Blender when a change is made.
del(bpy.app.handlers.render_pre[:])
del(bpy.app.handlers.render_post[:])
# Append the relevant functions to said list.
bpy.app.handlers.render_pre.append(prepare_rendering)
bpy.app.handlers.render_post.append(post_rendering)