# 
#
# ABOUT THIS SCRIPT:  This file is intended to process floor tiles rendered with Blender.
#
# STEPS:  Several things must be done before this script will really do some work.
#
#     1. Croppy (part of FreedroidRPG) must be compiled somewhere.
#
#     2. The path to the croppy executable must be entered below in the variable 'croppy_path'.
#
#     3. The output directory must be entered in the variable 'target_directory'.
#
#     4. The name of the floor tile being rendered must be entered in 'body_part_string'.
#
#     5. If using this script for the first time, PLEASE REFER TO
#	    http://www.freedroid.org/pmwiki/pmwiki.php/Main/RenderingGraphics
#        for usage instructions and explanations.
#
# HOW TO USE THE SCRIPT:  The script is linked to the frame change.  All that needs to
#                         be done is press the 'ANIM' button.
#

######################################################################
######################### PREPARE EVERYTHING #########################
######################################################################

######################
# Let's start with the preparations:  Importing modules...
#
import os
import sys
import bpy
import mathutils

######################
# Define some keywords and pathes that will affect
# output file generation...
#
croppy_path = "~/freedroid/croppy/croppy"
mv_path = "mv"
rm_path = "rm"

target_directory = "~/freedroid/graphics/tmp/"
body_part_string = "floor"

######################################################################
###################### RENAME AND CROPPY FUNCTION ####################
######################################################################

########################
# Now we check if maybe the last frame of the animation
# has been reached.  In this case we will do a rotation 
# of the camera.  If not we will start a croppy operation
# on the current output file.
#

def get_source_filename(scene):
	return scene.render.filepath + format(scene.frame_current, "04")

def do_rename_and_croppy(scene):
	source_filename = get_source_filename(scene) + ".png"
	target_filename = target_directory + "iso_" + body_part_string + "_" + format(scene.frame_current, "04") + ".png"

	parameter = " -i " + source_filename + " -o " + target_filename
	print("INVOKING COMMAND LINE:" , croppy_path , parameter)
	os.system(croppy_path + parameter)

###################################################################################
######################## offset computation functions #############################
###################################################################################

def compute_default_frustum(near, far, lens, aspect_ratio):
	half_size = 16 * near / lens
	if aspect_ratio > 1:
		size_x = half_size
		size_y = half_size / aspect_ratio
	else:
		size_x = half_size * aspect_ratio
		size_y = half_size
	return -size_x, size_x, -size_y, size_y, near, far

def compute_default_ortho(near, far, scale, aspect_ratio):
	half_size = scale / 2
	if aspect_ratio > 1:
		size_x = half_size
		size_y = half_size / aspect_ratio
	else:
		size_x = half_size * aspect_ratio
		size_y = half_size
	return -size_x, size_x, -size_y, size_y, -far, near

def get_frustum_matrix(left, right, bottom, top, near, far):
	return mathutils.Matrix([
		[2.0 * near / (right - left), 0.0, (right + left) / (right - left), 0.0],
		[0.0, 2.0 * near / (top - bottom), (top + bottom) / (top - bottom), 0.0],
		[0.0, 0.0, - (far + near) / (far - near), - (2 * far * near) / (far - near)],
		[0.0, 0.0, -1.0, 0.0]])

def get_ortho_matrix(left, right, bottom, top, near, far):
	return mathutils.Matrix([
		[2.0 / (right - left), 0.0, 0.0, - (right + left) / (right - left)],
		[0.0, 2.0 / (top - bottom), 0.0, - (top + bottom) / (top - bottom)],
		[0.0, 0.0, -2.0 / (far - near), -(far + near) / (far - near)],
		[0.0, 0.0, 0.0, 1.0]])

def get_perspective_matrix(camera, aspect_ratio):
	if camera.type == "ORTHO":
		left, right, bottom, top, near, far = compute_default_ortho(camera.clip_start, camera.clip_end, camera.ortho_scale, aspect_ratio)
		return get_ortho_matrix(left, right, bottom, top, near, far)
	elif camera.type == "PERSP":
		left, right, bottom, top, near, far = compute_default_frustum(camera.clip_start, camera.clip_end, camera.lens, aspect_ratio)
		return get_frustum_matrix(left, right, bottom, top, near, far)

def write_offset(scene, filename):
	res_x = scene.render.resolution_x * scene.render.resolution_percentage / 100
	res_y = scene.render.resolution_y * scene.render.resolution_percentage / 100
	pix_asp_x = scene.render.pixel_aspect_x
	pix_asp_y = scene.render.pixel_aspect_y

	aspect_ratio = 1.0
	if res_y != 0:
		aspect_ratio = (res_x * pix_asp_x) / (res_y * pix_asp_y)

	camera = bpy.data.objects["RotoCamera"]
	camera_mat = camera.matrix_world.copy()
	camera_mat.invert()

	camera = camera.data
	persp_mat = get_perspective_matrix(camera, aspect_ratio)

	offset_object = bpy.data.objects["offset"]
	offset_pos = offset_object.location.copy()
	offset_point = persp_mat * (camera_mat * offset_pos)
	xnd = offset_point.x
	ynd = offset_point.y
	center_x, center_y = (xnd + 1) * (res_x / 2.0), res_y - (ynd + 1) * (res_y / 2.0)
	offset_x, offset_y = -round(center_x), -round(center_y)

	with open(filename, 'w') as offset_file:
		offset_file.write("OffsetX=" + str(offset_x) + "\n")
		offset_file.write("OffsetY=" + str(offset_y) + "\n")

######################################################################

# Render and compute the offsets for the selected frames
def post_rendering(scene):
	write_offset(scene, get_source_filename(scene) + ".offset")
	do_rename_and_croppy(scene)

######################################################################
########################## 'MAIN PROGRAM' ############################
######################################################################

# Clear the list of functions to be run before and after every frame is rendered;
# thus it is not necessary to reload Blender when a change is made.
del(bpy.app.handlers.render_pre[:])
del(bpy.app.handlers.render_post[:])
# Append the relevant functions to said list.
bpy.app.handlers.render_post.append(post_rendering)
