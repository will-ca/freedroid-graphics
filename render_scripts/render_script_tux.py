#######################################################################
#
#	 CHECK_ROT.PY
#
#
#  This file is part of Freedroid
#
#  Freedroid is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  Freedroid is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Freedroid; see the file COPYING. If not, write to the 
#  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
#  MA  02111-1307  USA
#
# ABOUT THIS FILE:  This file is intended to make work with tux parts a
#		 bit simpler: it rotates the camera by 1/16th of a
#		 turn every X frames (X being the number of frames
#		 in the animation), and automatically names the
#		 images produced and passes them through croppy.
#
#
# STEPS:  Several things must be done before this script will really do some work.
#
# 1) Croppy (part of FreedroidRPG) must be compiled in the directory specified
#   in the variable 'croppy_path' found below.
#
# 2) The source_directory and target_directory variables found below must exist
#   (and also be writable for the user).
# 
# 3) The visible layer must be the one with the body part that needs
#   rendering. The different layers and their respective body parts are
#   as follows:
#
#   1. head
#   2. torso
#   3. weaponarm
#   4. shieldarm
#   5. feet
#
#   If a weapon is being rendered, any layer except for the first five
#   may be visible.
#
# 4) The variable body_part_string found below must be adjusted to the name
#   of the body part that needs rendering (named above) or of the weapon being
#   rendered.
#
# 5) If using this script for the first time, PLEASE REFER TO
#	http://www.freedroid.org/pmwiki/pmwiki.php/Main/RenderingGraphics
#    for usage instructions and explenations.
#
#----------------------------------------------------------------------



######################################################################
######################### PREPARE EVERYTHING #########################
######################################################################

######################
# Let's start with the preparations:  Importing modules...
#
import os
import sys
import bpy
import mathutils
from math import *

######################
# Define some constants for later computation
#
N_DIRECTIONS = 16
our_cam = bpy.data.objects['RotoCamera']

######################
# Define some file pathes...
#
croppy_path = "~/freedroid/croppy/croppy"
mv_path = "mv"
rm_path = "rm"
target_directory = "~/freedroid/graphics/tmp/"


######################
# The length of the full motion cycle is the last frame that still
# contains some keyframes of the animation.  It need not ever be changed
# unless we invent a completely new kind of Tux motion, like falling down
# and standing back up again or something, which would need to be appended
# after the running part of the motion.
#
full_motion_cycle = 35

stop_file_generation_after_image_nr = 35

######################################################################
################## FIND OUT THE OUTPUT DIRECTION #####################
######################################################################

######################
# Now we can start to identify the current direction
# by taking a closer look at the current camera position.
#
def get_output_direction():
	'''
	Return which direction the camera is at, as a number
	between 0 and N_DIRECTIONS-1 (inclusive).
	'''
	x, y = our_cam.location.x, our_cam.location.y
	# Protect against division by zero by losing unimportant accuracy.
	if abs(x) < 0.0001:
		x = 0.0001
	if x > 0:
		if y > 0:
			a = atan(y/x)
		else:
			a = 2*pi - atan(-y/x)
	else:
		if y > 0:
			a = pi - atan(y/-x)
		else:
			a = pi + atan(-y/-x)
	#print('angle (degrees) =', int(a * 180 / pi))
	a += pi / N_DIRECTIONS
	return int(a / (2*pi / N_DIRECTIONS)) % N_DIRECTIONS

def get_display_direction():
	return ( -4 - get_output_direction() ) % 16

print("BLENDER (MAIN): Final output direction: " , get_display_direction())

######################################################################
##################### OFFSET COMPUTATION FUNCTIONS ###################
######################################################################

def compute_default_frustum(near, far, lens, aspect_ratio):
	half_size = 16 * near / lens
	if aspect_ratio > 1:
		size_x = half_size
		size_y = half_size / aspect_ratio
	else:
		size_x = half_size * aspect_ratio
		size_y = half_size
	return -size_x, size_x, -size_y, size_y, near, far

def compute_default_ortho(near, far, scale, aspect_ratio):
	half_size = scale / 2
	if aspect_ratio > 1:
		size_x = half_size
		size_y = half_size / aspect_ratio
	else:
		size_x = half_size * aspect_ratio
		size_y = half_size
	return -size_x, size_x, -size_y, size_y, -far, near

def get_frustum_matrix(left, right, bottom, top, near, far):
	return mathutils.Matrix([
		[2.0 * near / (right - left), 0.0, (right + left) / (right - left), 0.0],
		[0.0, 2.0 * near / (top - bottom), (top + bottom) / (top - bottom), 0.0],
		[0.0, 0.0, - (far + near) / (far - near), - (2 * far * near) / (far - near)],
		[0.0, 0.0, -1.0, 0.0]])

def get_ortho_matrix(left, right, bottom, top, near, far):
	return mathutils.Matrix([
		[2.0 / (right - left), 0.0, 0.0, - (right + left) / (right - left)],
		[0.0, 2.0 / (top - bottom), 0.0, - (top + bottom) / (top - bottom)],
		[0.0, 0.0, -2.0 / (far - near), -(far + near) / (far - near)],
		[0.0, 0.0, 0.0, 1.0]])

def get_perspective_matrix(camera, aspect_ratio):
	if camera.type == "ORTHO":
		left, right, bottom, top, near, far = compute_default_ortho(camera.clip_start, camera.clip_end, camera.ortho_scale, aspect_ratio)
		return get_ortho_matrix(left, right, bottom, top, near, far)
	elif camera.type == "PERSP":
		left, right, bottom, top, near, far = compute_default_frustum(camera.clip_start, camera.clip_end, camera.lens, aspect_ratio)
		return get_frustum_matrix(left, right, bottom, top, near, far)

def write_offset(scene, filename):
	res_x = scene.render.resolution_x * scene.render.resolution_percentage / 100
	res_y = scene.render.resolution_y * scene.render.resolution_percentage / 100
	pix_asp_x = scene.render.pixel_aspect_x
	pix_asp_y = scene.render.pixel_aspect_y

	aspect_ratio = 1.0
	if res_y != 0:
		aspect_ratio = (res_x * pix_asp_x) / (res_y * pix_asp_y)

	camera = bpy.data.objects["RotoCamera"]
	camera_mat = camera.matrix_world.copy()
	camera_mat.invert()

	camera = camera.data
	persp_mat = get_perspective_matrix(camera, aspect_ratio)

	offset_object = bpy.data.objects["offset"]
	offset_pos = offset_object.location.copy()
	offset_point = persp_mat * (camera_mat * offset_pos)
	xnd = offset_point.x
	ynd = offset_point.y
	center_x, center_y = (xnd + 1) * (res_x / 2.0), res_y - (ynd + 1) * (res_y / 2.0)
	offset_x, offset_y = -round(center_x), -round(center_y)

	with open(filename, 'w') as offset_file:
		offset_file.write("OffsetX=" + str(offset_x) + "\n")
		offset_file.write("OffsetY=" + str(offset_y) + "\n")

######################################################################
##################### CAMERA ROTATION FUNCTION #######################
######################################################################

#######################
# Now we define our function that will rotate the camera
# by a fraction of a full turn
#
def do_cam_rot():
	'''
	Move the camera to the next of N_DIRECTIONS positions along
	a circle of radius 4.00 with constant Z.
	'''
	R = 4.00
	da = 2*pi / N_DIRECTIONS
	n_step = get_output_direction() - 1
	X_new = R * cos(da * n_step)
	Y_new = R * sin(da * n_step)

	our_cam.location.x = X_new
	our_cam.location.y = Y_new

	#our_cam.location.z = our_cam.RotZ - rot_angle

######################################################################
###################### RENAME AND CROPPY FUNCTION ####################
######################################################################

def get_source_filename(scene):
	return scene.render.filepath + format(scene.frame_current, "04")

def do_rename_and_croppy(scene):
	global target_directory
	#
	#--------------------
	# Now we find out the number code of the source image file
	# on the disk, that has been created by the blender by now.
	# With this we can fix the source file name now.
	#
	number_code = '%04d' % scene.frame_current
		
	source_filename = get_source_filename(scene) + ".png"
	print("BLENDER (do_rename_and_croppy): Source file name: " , source_filename)
	
	target_time = (scene.frame_current - 1) % full_motion_cycle + 1
	target_number_code = '%02d' % target_time
	#
	#--------------------
	# Now we fix the output number code.  The output number code can be derived from
	# the current camera position.
	#
	output_direction = get_display_direction()
	direction_code = '%02d' % output_direction
	#
	#--------------------
	# Now we should have all the information we need to assemble the final target
	# file name.
	#
	#target_filename = target_directory + subdirectory_string
	target_filename = target_directory
	target_filename = target_filename + "tux_rot_" + direction_code
	target_filename = target_filename + "_phase_" + target_number_code + ".png"
	print("BLENDER (do_rename_and_croppy): target file name would be: " , target_filename)
	#
	#--------------------
	# Now we can start the croppy operation.
	#
	if (( target_time <= stop_file_generation_after_image_nr )) :
		# ----
		command_line = croppy_path + " -o " + target_filename + " -i " + source_filename
		print("BLENDER (do_rename_and_croppy): invoking command line: " , command_line)
		os.system ( command_line )
		command_line = rm_path + " " + source_filename
		os.system ( command_line )
	else :
		print("BLENDER (do_rename_and_croppy): This image will not be needed by the engine ")
		print("						  and therefore we delete it.")
		command_line = rm_path + " " + source_filename
		os.system ( command_line )
		# ----
	print(" ")

######################################################################
########################## 'MAIN PROGRAM' ############################
######################################################################

def post_rendering(scene):
	write_offset(scene, get_source_filename(scene) + ".offset")
	do_rename_and_croppy(scene)
	if (scene.frame_current % full_motion_cycle) == 0:
		do_cam_rot()

# Clear the list of functions to be run before and after every frame is rendered;
# thus it is not necessary to reload Blender when a change is made.
del(bpy.app.handlers.render_pre[:])
del(bpy.app.handlers.render_post[:])
# Append the relevant functions to said list.
bpy.app.handlers.render_post.append(post_rendering)
