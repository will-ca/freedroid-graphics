--[[
 *
 *   Copyright (c) 2013 Samuel Degrande 
 *
 *
 *  This file is part of Freedroid
 *
 *  Freedroid is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Freedroid is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Freedroid; see the file COPYING. If not, write to the 
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 *  MA  02111-1307  USA
 *
 * This is a quick and dirty lua script that reads obstacle_specs.lua and
 * and obstacle_types enum in defs.h, and displays the correspondance
 * between the two sets.
 * Output example, when run from fdrpg top directory:
 * (lua print_obs_specs.lua ./map/obstacle_specs.lua ./src/defs.h)
 * 00 :                            iso_tree_stump.png 'ISO_TREE_4'
 * 01 :                          iso_wall_grey_ns.png 'ISO_V_WALL'
 * 02 :                          iso_wall_grey_ew.png 'ISO_H_WALL'
 * 03 :                   iso_wall_grey_handle_ns.png 'ISO_V_WALL_WITH_DOT'
 * 04 :                   iso_wall_grey_handle_ew.png 'ISO_H_WALL_WITH_DOT'
 *
]]--

local obstacles = {}
local iso_types = {}
local in_enum = false
local type_val = 0

function file_exists(name)
  local f=io.open(name,"r")
  if (f ~= nil) then
    io.close(f)
    return true
  else
    return false
  end
end

function obstacle(obs)
  table.insert(obstacles, obs)
end

if ((arg[1] == nil or not file_exists(arg[1])) or
    (arg[2] == nil or not file_exists(arg[2]))) then
  print(string.format("Usage: lua %s <obstacle_specs.lua filepath> <defs.h filepath>", arg[0]))
  os.exit(1)
end
 
dofile(arg[1])

defs = io.open(arg[2])

for str in defs:lines() do
  if (string.find(str, "enum obstacle_types {")) then
    break
  end
end

for str in defs:lines() do
  if (string.find(str, "};")) then
    defs:close()
    break
  else
   if (not string.find(str, "^%s*//")) then
      _, _, key, value = string.find(str, "^%s*([A-Z0-9_]+)%s*=%s*([%d]+)%s*")
      if (key) then
        type_val = tonumber(value)
        iso_types[type_val] = { name = key, used = false }
      else
        _, _, key = string.find(str, "^%s*([A-Z0-9_]+)")
        if (key) then
          type_val = type_val + 1
          iso_types[type_val] = { name = key, used = false }
        end
      end
    end
  end
end

for i = 1, #obstacles do
  local image_name = ""
  local iso_type = "NOT DEFINED"
  if (type(obstacles[i].image_filenames) == "table") then
    image_name = obstacles[i].image_filenames[1]
  else
    image_name = obstacles[i].image_filenames
  end
  if (iso_types[i-1]) then
    iso_types[i-1].used = true
    iso_type = iso_types[i-1].name
  end
  print(string.format("%.2d : %45s '%s'", i-1, image_name, iso_type))
end

local print_header = true

for i = 0, #iso_types do
  if (iso_types[i]) then
    if (iso_types[i].used == false) then
    if (print_header) then
      print("")
      print("Unused iso types:")
      print("-----------------")
      print_header = false
    end
      print(string.format("%.2d : '%s'", i, iso_types[i].name))
    end
  end
end
