#! /bin/sh
#set -x
oldname=$1
newname=$2

if [[ ! `git ls-files graphics/obstacles/ | grep "${oldname}\.png"` ]] ; then
	echo "Error, ${oldname} not found"
	exit 1
fi

if [[  `git ls-files graphics/obstacles/ | grep "${newname}\.png"` ]] ; then
	echo "Error, ${newname} already exists"
	git ls-files graphics/obstacles/ | grep "${newname}\.png"
	exit 2
fi

mv ./graphics/obstacles/${oldname}.png ./graphics/obstacles/${newname}.png
mv ./graphics/obstacles/${oldname}.offset ./graphics/obstacles/${newname}.offset

	echo "${oldname} =\> ${newname}"


if [[ `git ls-files graphics/obstacles/ | grep "shadow_${oldname}"` ]] ; then
	mv ./graphics/obstacles/shadow_${oldname}.png ./graphics/obstacles/shadow_${newname}.png
	mv ./graphics/obstacles/shadow_${oldname}.offset ./graphics/obstacles/shadow_${newname}.offset
	echo "shadow_${oldname} =\> shadow_${newname}"
fi


newname=`echo $newname | tr "/" "\\/"

sed -i s/$oldname/$newname/ ./map/obstacle_specs.lua
sed -i s/$oldname/$newname/ ./map/leveleditor_obstacle_categories.lua

git add -u
git add .
git commit -a -m "rename ${oldname}  to  ${newname} "
echo "Done"
