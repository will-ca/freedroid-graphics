#!/usr/bin/env python
# -*- coding: utf-8 -*-

from gimpfu import *
import sys, os


#guideLayers = gimp_image_get_layer_by_name(img, "names").children
#tiles = gimp_image_get_layer_by_name(img, "Background")

#pdb.gimp_image_get_layer_by_name(img, "names").visible = False

def crop_export(img, guideLayers, outputDirectory):
	pdb.gimp_image_undo_freeze(img);
	guideLayers.visible = False
	for i in guideLayers.children:
		newimg = pdb.gimp_image_duplicate(img);
		final = pdb.gimp_image_flatten(newimg);
		outputPath = os.path.join(outputDirectory, (i.name + ".png"));
		pdb.gimp_image_crop(newimg, i.width, i.height, i.offsets[0], i.offsets[1]);
		pdb.file_png_save(newimg, final, outputPath, i.name, 0, 8, 0, 0, 0, 0, 0);
		pdb.gimp_image_delete(newimg);
        guideLayers.visible = True
	pdb.gimp_image_undo_thaw(img);

### Registration; format taken from Ofnut's scripts
whoiam='\n'+os.path.abspath(sys.argv[0])
author='Infrared'
copyrightYear='2014'
desc='Crop an image composed of tiles according to named layers overlapping them'
register(
        'crop_export',
        desc+whoiam,
	desc,
        author,
	"The FreedroidRPG Project",
        copyrightYear,
        'Layer-guided tile export',
        '*',
        [
		#(PF_STRING, 'm', 'm', 'a'),
                (PF_IMAGE, 'img', 'Input image', None),
                (PF_DRAWABLE, 'guideLayers', 'Layer group containing guide layers', None),
                #(PF_DRAWABLE, 'scanningLayer', 'Scanning layer', None),
                #(PF_STRING, 'outputFileTemplate', 'Output file name template', 'scanner-image-%03d.png'),
                (PF_DIRNAME, 'outputDirectory', 'Output directory', "/tmp/")
        ],
        [],
        crop_export,
        menu='<Image>/Filters/Export'
)

main()
