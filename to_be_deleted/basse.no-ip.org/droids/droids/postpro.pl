#!/usr/bin/perl
# Copyright 2002 Chris Want and Guillermo S. Romero
# Distribute under GPL
# Credits:
#  First skeleton by GSR
#  Video processing skeleton by CW
#  This transformation by CW

use Gimp;
Gimp::init;

use File::Basename;
$progname  = basename($0);

%default = 
    (
     gauss_x      => 5.0,
     gauss_y      => 5.0,
     noise_level  => 0.60,
     contrast     => 90,
     effect_level => 50
     );

### Process arguments
use Getopt::Long;
Getopt::Long::Configure ("bundling");  
GetOptions("pattern-in|i=s"   => \$pattern_in,
           "start-in|I=i"     => \$start_in,
           "pattern-out|o=s"  => \$pattern_out,
           "start-out|O=i"    => \$start_out,
           "num-frames|n=i"   => \$num_frames,
           "gauss-x|x=s"      => \$gauss_x,
           "gauss-y|y=s"      => \$gauss_y,
           "noise-level|l=s"  => \$noise_level,
           "contrast|c=s"     => \$contrast,
           "effect-level|e=s" => \$effect_level,
           "help|h"           => \$help);

arg_error("")                               if $help;
arg_error("-i or --pattern-in required!")   if !$pattern_in;
arg_error("-I or --start-in required!")     if !$start_in;
arg_error("-o or --pattern-out required!")  if !$pattern_out;
arg_error("-O or --start-out required!")    if !$start_out;
arg_error("-n or --num-frames required!")   if !$num_frames;
$gauss_x      = $default{"gauss_x"}         if !$gauss_x;
$gauss_y      = $default{"gauss_y"}         if !$gauss_y;
$noise_level  = $default{"noise_level"}     if !$noise_level;
$contrast     = $default{"contrast"}        if !$contrast;
$effect_level = $default{"effect_level"}    if !$effect_level;

$got_noise  = "FALSE";

### Loop through the frames applying the filter
for ($i = 0; $i < $num_frames; ++$i) {       
    $file_in  = sprintf $pattern_in,  $start_in  + $i;
    $file_out = sprintf $pattern_out, $start_out + $i;

    ### level is between 0 and 1 (not needed for this filter).
    $level    = $i / ($num_frames + 1);
    ### Paskaks($file_in, $file_out, $level);
    Kiva($file_in, $file_out, $level);
    $percent = Round($level * 100);
    print "$progname: $percent% done\n";
}

sub Kiva {
    my ($file_in, $file_out, $level) = @_;
    my $img = Gimp->file_load($file_in, $file_in);
    my ($width, $height) = ($img->width, $img->height);
    
    $drw = $img->active_drawable;
    
    
    ###$drwOho=$drw->copy(1);
    ###$drwOho->hue_saturation(0, 0, 0, 10);
    
    
    my $blurLayer=$drw->copy(1);
    $img->add_layer($blurLayer, -1);
    $blurLayer->gauss_rle2(12, 12);
    $blurLayer->set_mode(MULTIPLY_MODE);
    $blurLayer->set_opacity(55);
    
    $drw = $img->flatten;
    
    my $glowLayer=$drw->copy(1);
    $img->add_layer($glowLayer, -1);
    $glowLayer->gauss_rle2(12, 12);
    $glowLayer->set_mode(ADDITION_MODE);
    $glowLayer->set_opacity(22);
    
    $drw = $img->flatten;
    
    my $borderLayer=$drw->copy(1);
    $img->add_layer($borderLayer, -1);
    $borderLayer->brightness_contrast(-100, 75);
    $img->selection_all;
    $img->selection_shrink(20);
    $img->selection_feather(99);
    $borderLayer->edit_cut;
    $borderLayer->set_mode(MULTIPLY_MODE);
    $borderLayer->set_opacity(35);
        
    $drw = $img->flatten;

    Gimp->file_save($drw, $file_out, $file_out);
    $img->delete;
    
}

sub Paskaks {
    my ($file_in, $file_out, $level) = @_;
    my $img = Gimp->file_load($file_in, $file_in);
    my ($width, $height) = ($img->width, $img->height);

    $drw = $img->active_drawable;
    
    my $layer2 = $drw->copy(1);
    $img->add_layer($layer2, -1);
    $layer2->threshold(127, 255);
    $drw = $img->flatten;
    Gimp->file_save($drw, $file_out, $file_out);
    $img->delete;
}

sub FilmNoir {
    my ($file_in, $file_out, $level) = @_;

    ### load image from input file
    my $img = Gimp->file_load($file_in, $file_in);
    my ($width, $height) = ($img->width, $img->height);
    
    $drw = $img->active_drawable;
    $drw->desaturate;

    if ($got_noise eq "FALSE") {
        $drwnoise = $drw->copy(1);

        $img->selection_all;
        $drwnoise->edit_clear;
        $drwnoise->noisify(0, $noise_level, $noise_level, $noise_level, 1.0);
        $drwnoise->edit_copy;

        $drwnoise->delete;

        $got_noise ="TRUE";
    }

    my $layer2 = $drw->copy(1);

    $img->add_layer($layer2, -1);
    $layer2->brightness_contrast(0, $contrast);

    my $layer3 = $layer2->copy(1);
    if ($got_noise == "TRUE") {
        $layer2->edit_paste(0)->anchor;
    }
    $layer2->gauss_rle2($gauss_x, $gauss_y);

    $layer2->desaturate;

    $layer2->set_opacity($effect_level);

    $img->add_layer($layer3, -1);
    $layer3->set_mode(OVERLAY_MODE);
    $drw = $img->flatten;

    Gimp->file_save($drw, $file_out, $file_out);
    $img->delete;
}


sub Round {
    ### Yep -- it rounds numbers
    my ($float) = @_;
    return (int($float+0.5));
}

sub arg_error {
    ### Display errors in arguments and usage
    my ($message) = @_;
    if ($message ne "") {
        print "$progname: Los parametros elegidos son incorrectos!\n\n";
        print "*** $message ***\n";
    }
    print <<"EOUSAGE";

Description: $progname processes the input frames to look like
  they are in a Film Noir black & white style.

Usage: $progname [options]

Options:

  -i or --pattern-in (required)
      The printf formatted pattern for the input frames.

  -I or --start-in (required)
      The starting frame number for the input frames.

  -o or --pattern-out (required)
      The printf formatted pattern for the output frames.

  -O or --start-out (required)
      The starting frame number for the output frames A.

  -n or --num-frames (required)
      The number of frames tp process.

  -x or --gauss-x (optional, default = 5.0)
      The x value for gaussian rle blur for the film grain

  -y or --gauss-y (optional, default = 5.0)
      The y value for gaussian rle blur for the film grain

  -l or --noise-level (optional, default = 0.60)
      The noisiness of the film grain

  -c or --contrast (optional, default = 90)
      This setting is used to boost the black and white tones.

  -e or --effect_level (optional, default = 50)
      This setting can be used to bring out the film grain and
      contrast more.

  -h or --help (optional)
      Print usage message and exit

Example:

  $progname -i in%d.jpg -I 31 -o out%06d.png -O 47 -n 500

  -- processes input frames in31.tga ... in530.tga and writes 
     to frames out000047.tga ... out000546.tga.

EOUSAGE

exit 1;

}
